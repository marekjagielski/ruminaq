[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=alert_status)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=security_rating)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=sqale_index)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)


[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=ncloc)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=coverage)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)


[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=bugs)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=code_smells)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=org.ruminaq%3Aorg.ruminaq.root&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=org.ruminaq%3Aorg.ruminaq.root)
