package org.ruminaq.tasks.embeddedtask.ui;

public interface IEmbeddedTaskUiApi {
  String getModelerVersion();
}
