package org.ruminaq.tasks.pythontask.ui.wizards;

public interface CreatePythonTaskListener {
  void created(String path);
}
