package org.ruminaq.tasks.pythontask.ui;

import org.ruminaq.tasks.pythontask.ui.wizards.ICreatePythonTaskPage;

public interface IPythonTaskUiApi {
  ICreatePythonTaskPage getCreatePythonTaskPage();
}
