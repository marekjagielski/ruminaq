package org.ruminaq.runner;

public class RunnerException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public RunnerException(String msg) {
    super(msg);
  }

}
