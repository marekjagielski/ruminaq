package org.ruminaq.runner.impl.debug.events.debugger;

import org.ruminaq.runner.impl.debug.events.AbstractEvent;
import org.ruminaq.runner.impl.debug.events.IDebuggerEvent;

public class SuspendedEvent extends AbstractEvent implements IDebuggerEvent {

  private static final long serialVersionUID = 1L;
}
