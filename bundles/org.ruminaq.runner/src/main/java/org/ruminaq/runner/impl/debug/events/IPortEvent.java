package org.ruminaq.runner.impl.debug.events;

public interface IPortEvent {
  String getDiagramPath();

  String getTaskId();

  String getPortId();
}
