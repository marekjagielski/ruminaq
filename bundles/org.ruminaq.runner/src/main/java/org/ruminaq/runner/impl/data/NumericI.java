package org.ruminaq.runner.impl.data;

import java.util.List;

public abstract class NumericI extends DataI {

  private static final long serialVersionUID = 1L;

  protected NumericI(List<Integer> dims) {
    super(dims);
  }

}
