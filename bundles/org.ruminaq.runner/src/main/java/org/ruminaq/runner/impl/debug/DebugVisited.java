package org.ruminaq.runner.impl.debug;

public interface DebugVisited {
  void accept(DebugVisitor visitor);
}
