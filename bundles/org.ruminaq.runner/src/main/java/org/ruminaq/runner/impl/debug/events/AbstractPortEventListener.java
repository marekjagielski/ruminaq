package org.ruminaq.runner.impl.debug.events;

public interface AbstractPortEventListener {
  String getDiagramPath();

  String getTaskId();

  String getPortId();
}
