package org.ruminaq.runner.util;

public interface Observer {

  void update(Observable o, Object obj);

}
