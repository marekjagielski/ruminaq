package org.ruminaq.runner.impl;

public class ImplementationException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ImplementationException(String msg) {
    super(msg);
  }
}
