package org.ruminaq.runner.dirmi;

public interface RegistrationDoneListener {
  void registrationDone();

  void debugInitialized();
}
