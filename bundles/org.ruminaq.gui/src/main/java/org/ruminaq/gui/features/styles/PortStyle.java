/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

package org.ruminaq.gui.features.styles;

import org.eclipse.graphiti.mm.algorithms.styles.Style;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;
import org.ruminaq.util.StyleUtil;

public class PortStyle {

  private static final IColorConstant PORT_FOREGROUND = new ColorConstant(0, 0,
      0);
  private static final IColorConstant PORT_BACKROUND = new ColorConstant(255,
      255, 255);

  public static Style getStyle(Diagram diagram) {
    final String styleId = "PORT"; //$NON-NLS-1$

    Style style = StyleUtil.findStyle(diagram, styleId);
    if (style == null) {
      IGaService gaService = Graphiti.getGaService();
      style = gaService.createStyle(diagram, styleId);
      style.setForeground(gaService.manageColor(diagram, PORT_FOREGROUND));
      style.setBackground(gaService.manageColor(diagram, PORT_BACKROUND));

    }
    return style;
  }

}
