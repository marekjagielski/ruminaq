/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

/**
 * Wizard to create new Ruminaq eclipse project.
 *
 * @author Marek Jagielski
 */
package org.ruminaq.eclipse.wizards.project;
