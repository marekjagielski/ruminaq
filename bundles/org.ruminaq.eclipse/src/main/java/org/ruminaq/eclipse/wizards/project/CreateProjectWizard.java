/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

package org.ruminaq.eclipse.wizards.project;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.project.IProjectConfigurationManager;
import org.eclipse.m2e.core.project.MavenUpdateRequest;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.osgi.framework.Version;
import org.ruminaq.eclipse.Image;
import org.ruminaq.eclipse.Messages;
import org.ruminaq.eclipse.RuminaqException;
import org.ruminaq.eclipse.api.EclipseExtension;
import org.ruminaq.eclipse.wizards.diagram.CreateDiagramWizard;
import org.ruminaq.eclipse.wizards.diagram.CreateDiagramWizardNamePage;
import org.ruminaq.logs.ModelerLoggerFactory;
import org.ruminaq.prefs.ProjectProps;
import org.ruminaq.util.EclipseUtil;
import org.ruminaq.util.ImageUtil;
import org.ruminaq.util.PlatformUtil;
import org.ruminaq.util.ServiceUtil;
import org.slf4j.Logger;

/**
 * Wizard to create new Ruminaq eclipse project.
 *
 * @author Marek Jagielski
 */
public class CreateProjectWizard extends BasicNewProjectResourceWizard {

  private static final Logger LOGGER = ModelerLoggerFactory
      .getLogger(CreateProjectWizard.class);

  public static final String ID = CreateProjectWizard.class.getCanonicalName();

  private static final String BASIC_NEW_PROJECT_PAGE_NAME = "basicNewProjectPage";

  public static final String PROPERTIES_FILE = SourceFolders.MAIN_RESOURCES
      + "/ruminaq.properties";

  private static final String OUTPUT_CLASSES = "target/classes";

  public static final String MAIN_MODULE = "MAIN_MODULE";

  public static final String BIN_DIRECTORY = "bin";

  /**
   * Sets the window title.
   *
   * @see org.eclipse.jface.wizard.Wizard#setWindowTitle()
   */
  @Override
  public void setWindowTitle(final String newTitle) {
    super.setWindowTitle(Messages.createProjectWizardWindowTitle);
  }

  /**
   * New project wizard.
   *
   * @see org.eclipse.jface.wizard.Wizard#createPageControls(org.eclipse.swt.widgets.Composite)
   */
  @Override
  public void createPageControls(Composite pageContainer) {

    super.createPageControls(pageContainer);

    WizardNewProjectCreationPage basicNewProjectPage = (WizardNewProjectCreationPage) getPage(
        BASIC_NEW_PROJECT_PAGE_NAME);
    basicNewProjectPage.setTitle(Messages.createProjectWizardTitle);
    basicNewProjectPage.setImageDescriptor(
        ImageUtil.getImageDescriptor(Image.RUMINAQ_LOGO_64X64));
    basicNewProjectPage.setDescription(Messages.createProjectWizardDescription);
  }

  /**
   * Creating project. If smth wrong it shows modal window.
   *
   * @see org.eclipse.jface.wizard.Wizard#performFinish()
   */
  @Override
  public boolean performFinish() {
    if (super.performFinish()) {
      IProject newProject = getNewProject();

      try {
        Nature.setNatureIds(newProject);
        SourceFolders.createSourceFolders(newProject);

        IJavaProject javaProject = JavaCore.create(newProject);
        new JavaClasspathFile().setClasspathEntries(javaProject);
        createOutputLocation(javaProject);

        new PomFile().createPomFile(newProject);
        Builders.configureBuilders(newProject);
        createPropertiesFile(newProject);

        Version version = PlatformUtil.getBundleVersion(this.getClass());
        ProjectProps.getInstance(newProject).put(ProjectProps.RUMINAQ_VERSION,
            new Version(version.getMajor(), version.getMinor(),
                version.getMicro()).toString());

        deleteBinDirectory(newProject);

        ServiceUtil
            .getServicesAtLatestVersion(CreateProjectWizard.class,
                EclipseExtension.class)
            .stream()
            .forEach(e -> e.createProjectWizardPerformFinish(javaProject));

        updateProject(newProject);

        return true;

      } catch (RuminaqException e) {
        LOGGER.error(Messages.createProjectWizardFailed, e);
        MessageDialog.openError(getShell(), Messages.ruminaqFailed,
            e.getMessage());
      }
    }

    return false;
  }

  /**
   * Newly created project by default has output directory bin. Eclipse will
   * manage to create this directory before we change classpath file.
   *
   * @param projet eclipse project reference
   * @throws RuminaqException smth went wrong
   */
  private static void deleteBinDirectory(IProject projet)
      throws RuminaqException {
    try {
      EclipseUtil.deleteProjectDirectoryIfExists(projet, BIN_DIRECTORY);
    } catch (CoreException e) {
      throw new RuminaqException(Messages.createProjectWizardFailed, e);
    }
  }

  private static void updateProject(IProject project) throws RuminaqException {
    IProjectConfigurationManager configurationManager = MavenPlugin
        .getProjectConfigurationManager();
    MavenUpdateRequest request = new MavenUpdateRequest(project, true, true);
    try {
      configurationManager.updateProjectConfiguration(request,
          new NullProgressMonitor());
    } catch (CoreException e) {
      throw new RuminaqException(Messages.createProjectWizardFailed, e);
    }
  }

  private static void createOutputLocation(IJavaProject javaProject)
      throws RuminaqException {
    IPath targetPath = javaProject.getPath().append(OUTPUT_CLASSES);
    try {
      javaProject.setOutputLocation(targetPath, null);
    } catch (JavaModelException e) {
      throw new RuminaqException(Messages.createProjectWizardFailed, e);
    }
  }

  private static void createPropertiesFile(IProject project)
      throws RuminaqException {
    Properties prop = new Properties();
    try (OutputStream output = new ByteArrayOutputStream()) {
      prop.setProperty(MAIN_MODULE,
          SourceFolders.TASK_FOLDER + "/"
              + CreateDiagramWizardNamePage.DEFAULT_DIAGRAM_NAME
              + CreateDiagramWizard.DIAGRAM_EXTENSION_DOT);
      prop.store(output, null);
      IFile outputFile = project.getFile(PROPERTIES_FILE);
      outputFile.create(
          new ByteArrayInputStream(
              ((ByteArrayOutputStream) output).toByteArray()),
          true, new NullProgressMonitor());
    } catch (IOException | CoreException e) {
      throw new RuminaqException(
          Messages.createProjectWizardFailedPropertiesFile, e);
    }
  }
}
