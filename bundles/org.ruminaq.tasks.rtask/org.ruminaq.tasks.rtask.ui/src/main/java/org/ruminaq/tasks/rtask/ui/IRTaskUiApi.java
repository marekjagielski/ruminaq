package org.ruminaq.tasks.rtask.ui;

import org.ruminaq.tasks.rtask.ui.wizards.ICreateRTaskPage;

public interface IRTaskUiApi {
  ICreateRTaskPage getCreatePythonTaskPage();
}
