package org.ruminaq.tasks.rtask.ui.wizards;

public interface CreateRTaskListener {
  void created(String path);
}
