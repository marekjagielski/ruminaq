package org.ruminaq.tasks;

public interface SicWindow {
  void requestFocus();

  void init(Object o);
}
