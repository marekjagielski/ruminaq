package org.ruminaq.tasks.javatask.ui;

import org.ruminaq.tasks.javatask.ui.wizards.ICreateJavaTaskPage;

public interface IJavaTaskUiApi {
  ICreateJavaTaskPage getCreateJavaTaskPage();
}
