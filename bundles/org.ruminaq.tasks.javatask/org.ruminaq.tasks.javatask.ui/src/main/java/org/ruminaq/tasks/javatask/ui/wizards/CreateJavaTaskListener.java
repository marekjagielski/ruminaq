package org.ruminaq.tasks.javatask.ui.wizards;

import org.eclipse.jdt.core.IType;

public interface CreateJavaTaskListener {
  void created(IType createdElement);
}
