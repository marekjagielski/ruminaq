package org.ruminaq.model.desc;

public enum Position {
  TOP, LEFT, BOTTOM, RIGHT;
}
