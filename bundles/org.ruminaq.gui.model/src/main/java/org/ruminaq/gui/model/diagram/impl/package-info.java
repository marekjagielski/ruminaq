/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

/**
 * Base classes for overriding Graphiti pictogram model.
 *
 * @author Marek Jagielski
 */
package org.ruminaq.gui.model.diagram.impl;
