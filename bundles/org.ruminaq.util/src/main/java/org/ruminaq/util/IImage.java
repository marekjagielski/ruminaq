package org.ruminaq.util;

public interface IImage {
  String getPath();
}
