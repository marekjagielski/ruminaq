package org.ruminaq.util;

import java.util.List;

public interface ServiceFilterArgs {
  List<?> getArgs();
}
