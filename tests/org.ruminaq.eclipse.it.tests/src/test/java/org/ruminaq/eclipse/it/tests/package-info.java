/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

/**
 * Test classes for eclipse project.
 * <p>
 * These tests should check if Ruminaq is correctly installed and that it is
 * possible to create a diagram
 * </p>
 *
 * @author Marek Jagielski
 */
package org.ruminaq.eclipse.it.tests;
