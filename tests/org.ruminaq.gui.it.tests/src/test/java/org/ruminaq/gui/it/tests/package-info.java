/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/

/**
 * Test classes for gui project.
 * <p>
 * These tests should check if Diagram Ruminaq works
 * </p>
 *
 * @author Marek Jagielski
 */
package org.ruminaq.gui.it.tests;
